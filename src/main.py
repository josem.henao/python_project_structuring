import argparse
import json

from src.process.process_executor import ProcessExecutor

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Runs a workflow process created under the process layer", )
    parser.add_argument("-p", "--process_name",
                        help="The name of the registered process at PROCESS in config file",
                        required=True
                        )
    parser.add_argument("-f", "--context_file",
                        help="The json file path with some extra arguments for the workflow to run",
                        required=False
                        )
    parser.add_argument("-e", "--environment",
                        help="Environment where will be executed the process (local, dev, prod)",
                        required=False,
                        choices=["local", "dev", "prod"]
                        )

    args = parser.parse_args()

    if args.context_file:
        with open(args.context_file) as file:
            context = json.load(file)
    else:
        context = {}

    if args.process_name:
        ProcessExecutor.run_process(**{
            'process_name': args.process_name,
            'environment': args.environment,
            **context
        })
    else:
        raise Exception("You must provide a valid process name")
