FROM python:3.7.6-stretch

WORKDIR /src

COPY . .

RUN pip install pipenv
RUN pipenv uninstall --all
RUN pipenv install

